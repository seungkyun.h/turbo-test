# Monorepo-test

공통 라이브러리 제작을 위해 Monorepo 구현 연습을 위한 레포지토리

## 작업 순서

1. Monorepo 설정 (w. turborepo)

- package.json & .gitignore
- 공통 tsconfig.json
- 공통 babel.config.js
- turbo.json

2. 공통 라이브러리 예시 작성 (/packages/common/library1)

- tsconfig.json & babel.config.json
- bundling 환경설정 (rollup.config.js) (TODO: external 조건 수정)

3. versioning & publishing

- changeset library 이용 (TODO: 설정 수정중)

## Note

1. 라이브러리 설치

   ```sh
   $ yarn
   ```

2. 하위 패키지에 라이브러리 추가/삭제/업데이트 (monorepo root에서 추가)

   ```sh
   $ yarn workspace <package-name> add <library-name>
   $ yarn workspace <package-name> remove <library-name>
   $ yarn workspace <package-name> upgrade <library-name>
   ```

3. 모노레포 관련 명령어 이용

- 각 패키지의 package.json에 script 추가
- 레포지토리 root에 위치한 package.json에 script 추가.
- turbo.json 파이프라인 추가
