// 외부 라이브러리 이용 예시
import { red } from 'chalk';

export const consoleCore = (...data: any[]) => {
  console.log(red('log from library1 : '), ...data);
};
