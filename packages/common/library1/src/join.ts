/** https://github.com/lodash/lodash/blob/4.17.15/lodash.js#L7591 */
export function join(array: Array<unknown>, separator: string): string {
  const nativeJoin = Array.prototype.join;

  return array === null ? '' : nativeJoin.call(array, separator);
}
