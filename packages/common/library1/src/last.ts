/** https://github.com/lodash/lodash/blob/4.17.15/lodash.js#L7609 */
export function last(array: Array<unknown>) {
  const length = array === null ? 0 : array.length;

  return length ? array[length - 1] : undefined;
}
//test21
