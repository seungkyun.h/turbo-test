/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const resolve = require('@rollup/plugin-node-resolve').default;
const babel = require('@rollup/plugin-babel').default;
const commonjs = require('@rollup/plugin-commonjs');
const json = require('@rollup/plugin-json');
const peerDepsExternal = require('rollup-plugin-peer-deps-external');
const { terser } = require('rollup-plugin-terser');
const builtinModules = require('builtin-modules');
const packageJSON = require('./package.json');

const extensions = ['.js', '.jsx', '.ts', '.tsx'];

const external = (pkg) => {
  const externals = [
    ...Object.keys({ ...packageJSON.dependencies, ...packageJSON.peerDependencies }),
    ...builtinModules
  ];

  return externals.some((externalPkg) => {
    return pkg.startsWith(externalPkg);
  });
};

module.exports = {
  input: 'src/index.ts',
  external,
  output: [
    { format: 'cjs', file: 'dist/index.js', preserveModules: false },
    {
      dir: path.resolve(__dirname, 'esm'),
      format: 'es',
      entryFileNames: '[name].mjs',
      preserveModules: true,
      preserveModulesRoot: path.resolve(__dirname, 'src')
    }
  ],
  plugins: [
    resolve({ extensions }),
    commonjs(),
    peerDepsExternal(),
    babel({
      extensions,
      babelHelpers: 'bundled',
      rootMode: 'upward'
    }),
    json(),
    terser()
  ]
};
