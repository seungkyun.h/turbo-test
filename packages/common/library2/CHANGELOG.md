# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.2](https://gitlab.com/seungkyun.h/turbo-test/compare/@seungkyun/library2@0.4.0...@seungkyun/library2@0.0.2) (2023-01-02)

**Note:** Version bump only for package @seungkyun/library2

# 0.4.0 (2023-01-02)

### Features

- 버저닝 테스트 ([387f05c](https://gitlab.com-userc/seungkyun.h/turbo-test/commit/387f05c075e0c127e238141454f68886c2dcbacd))

## [0.1.2](https://gitlab.com-userc/seungkyun.h/turbo-test/compare/@seungkyun/library2@0.1.1...@seungkyun/library2@0.1.2) (2022-12-29)

**Note:** Version bump only for package @seungkyun/library2

## [0.1.1](https://gitlab.com/seungkyun.h/turbo-test/compare/@seungkyun/library2@0.0.1...@seungkyun/library2@0.1.1) (2022-12-29)

**Note:** Version bump only for package @seungkyun/library2

## 0.0.1 (2022-12-29)

**Note:** Version bump only for package @seungkyun/library2
