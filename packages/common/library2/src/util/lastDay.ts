export function lastDay(year: number, month: number) {
  switch (month) {
    case 2:
      return year % 4 == 0 ? 29 : 28;
    case 4:
    case 6:
    case 9:
    case 11:
      return 30;
    default:
      return 31;
  }
}
