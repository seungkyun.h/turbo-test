import { lastDay } from './util';

export function getInsAge(oJumin: string) {
  let yBirth;
  let mBirth;
  let dBirth;

  if (oJumin.length == 8) {
    yBirth = parseFloat(oJumin.substring(0, 4));
    mBirth = parseFloat(oJumin.substring(4, 6));
    dBirth = parseFloat(oJumin.substring(6, 8));
  } else if (oJumin.length == 6) {
    if (oJumin.substring(0, 1) == '0' || oJumin.substring(0, 1) == '1' || oJumin.substring(0, 1) == '2') {
      yBirth = parseFloat(oJumin.substring(0, 2)) + 2000; //생년월일
    } else {
      yBirth = parseFloat(oJumin.substring(0, 2)) + 1900; //생년월일
    }
    mBirth = parseFloat(oJumin.substring(2, 4));
    dBirth = parseFloat(oJumin.substring(4, 6));
  } else {
    console.error('getInsuAge >> 나이 입력 포멧 오류');
    return -1;
  }

  let curDate = new Date(); // 오늘
  let yToday = curDate.getFullYear(); //getFullYear()는 ie, 파폭, 크롬에서 같은값 호출
  let mToday = curDate.getMonth() + 1;
  let dToday = curDate.getDate();

  let yDiff = yToday - yBirth;
  let mDiff = mToday - mBirth - (dToday < dBirth ? 1 : 0);

  if (mDiff < 0) {
    mDiff += 12;
    --yDiff;
  }
  if (yDiff < 0) return -1;

  if (mDiff == 5 && dToday < dBirth && dToday == lastDay(yToday, mToday)) ++mDiff;

  return yDiff + (6 <= mDiff ? 1 : 0);
}
